package com.example.erez1.androidfinalproject;

import android.app.VoiceInteractor;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * This class is used in order to communicate with web api
 * This class make use of Volley libary in order to make json requests
 * This class parse json result into Question object
 */

public class QuestionsApiHandler extends AsyncTask<Void, Void, String> {
    private String API_URL;
    private RequestQueue req;
    private Context context;
    private ArrayList<Question> questions;
    public QuestionListAsyncResponse delegate = null;

    /**
     * Constructs QuestionsApiHandler object
     *
     * @param context    the activity that the constructor activated from
     * @param categoryID the category of questions selected
     * @param level      the difficulty level of the questions selected
     * @param amount     the amount of questions selected
     */
    public QuestionsApiHandler(Context context, GameMatcher gameMatcher) {
            this.API_URL = gameMatcher.getCategoryId()!= -1 ?
                String.format("https://opentdb.com/api.php?amount=%d&category=%d&difficulty=%s&type=multiple", gameMatcher.getQuestionAmount()
                        , gameMatcher.getCategoryId(), gameMatcher.getDifficulty())
                : String.format("https://opentdb.com/api.php?amount=%d&difficulty=%s&type=multiple",
                    gameMatcher.getQuestionAmount(), gameMatcher.getDifficulty()); // incase of random
        this.context = context;
        this.req = Volley.newRequestQueue(this.context);
        this.questions = new ArrayList<Question>();
    }

    /**
     * take json data and parse into Question object
     *
     * @param jString string that contains json format data
     * @return Question object created from json data
     */
    private Question parseJsonQuestion(String jString) {
        try {
            JSONObject js = new JSONObject(jString);
            String category = js.getString("category");
            String difficulty = js.getString("difficulty");
            String question = js.getString("question");
            question = question.replace("&quot;", "'");
            question = question.replace("&#039;", "'");
            question = question.replace("&amp;", "'");
            String correctAnswer = js.getString("correct_answer");
            JSONArray arrJson = js.getJSONArray("incorrect_answers");
            ArrayList<String> wrongAnswers = new ArrayList<String>(arrJson.length());
            for (int i = 0; i < arrJson.length(); i++)
                wrongAnswers.add(i,arrJson.getString(i));
            Question que = new Question(question, correctAnswer, wrongAnswers, category, difficulty);
            return que;
        } catch (JSONException exc) {
            Log.e("JsonError", exc.getMessage());
            return null;
        }
    }

    /**
     * make web api request to the url
     * parse data
     *
     * @param delegate interface to call when task finishes
     */
    public void getQuestions(QuestionListAsyncResponse delegate) {
        this.questions.clear();
        this.delegate = delegate;
        JsonObjectRequest j = new JsonObjectRequest(Request.Method.GET, this.API_URL, "",
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Iterator<String> keys = response.keys();
                        String key;
                        // skip unwanted json attributes
                        key = keys.next();
                        key = keys.next();
                        try {
                            JSONArray arr = response.getJSONArray(key);
                            for (int i = 0; i < arr.length(); i++) {
                                Question q = parseJsonQuestion(arr.get(i).toString());
                                QuestionsApiHandler.this.questions.add(q);
                            }
                            onPostExecute(questions);
                        } catch (JSONException error) {
                            Log.e("Error", error.getMessage());
                        }

                    }
                }, new Response.ErrorListener()

        {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VolleyError", error.getMessage());
            }
        });
        this.req.add(j);
    }

    /**
     * not in use
     *
     * @param voids
     * @return null
     */
    @Override
    protected String doInBackground(Void... voids) {
        return null;
    }

    /**
     * called when async task finishes
     * activate interface method
     *
     * @param s the questions arraylist result
     */
    protected void onPostExecute(ArrayList<Question> s) {
        this.delegate.processFinish(s);
    }
}
