package com.example.erez1.androidfinalproject;

/**
 * This class represent the category of the question
 * has an id and name
 * used for attaching id to name in order to request it from the web api
 */

public class Category {
    private int id;
    private String name;

    /**
     * construct Category object
     *
     * @param id   the category id
     * @param name the category name
     */
    public Category(int id, String name) {
        this.id = id;
        this.name = name;
    }

    /**
     * for firebase use
     */
    public Category() {
    }


    /**
     * get category id
     * @return category id
     */
    public int getId() {
        return id;
    }

    /**
     * set category id
     *
     * @param id category id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * get category name
     * @return category name
     */
    public String getName() {
        return name;
    }

    /**
     * set category name
     * @param name category name
     */
    public void setName(String name) {
        this.name = name;
    }
}
