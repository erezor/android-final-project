package com.example.erez1.androidfinalproject;


import java.util.ArrayList;

/**
 * this interface is used to represent async response
 */
public interface QuestionListAsyncResponse {
    /**
     * called when async task finished
     * @param questions the result - arraylist of questions
     */
    void processFinish(ArrayList<Question> questions);
}