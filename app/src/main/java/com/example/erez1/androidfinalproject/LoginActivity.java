package com.example.erez1.androidfinalproject;


import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.database.Query;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This Activity is used for logging into the application
 * The activity connect with firebase in order to retrieve all users and check for valid credentials
 */
public class LoginActivity extends AppCompatActivity {
    private EditText inputEmail, inputPassword;
    private TextView tvError;
    private Button btnLogin, btnSignup;
    private String userEmail, userPassword;
    private FirebaseDatabase database;
    private DatabaseReference myRef;
    private Boolean found;
    public static final String regex = "^(.+)@(.+)$";
    private CheckBox cbKeep;

    /**
     * creates the UI of the activity
     * set up buttons listener
     * create loggedUser instance after login has been verified
     *
     * @param savedInstanceState helps to create GUI
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        inputEmail = (EditText) findViewById(R.id.input_email);
        inputPassword = (EditText) findViewById(R.id.input_password);
        btnLogin = (Button) findViewById(R.id.btn_login);
        btnSignup = (Button) findViewById(R.id.btn_signup);
        tvError = (TextView) findViewById(R.id.tv_error);
        cbKeep = (CheckBox) findViewById(R.id.cb_keep);

        database = FirebaseDatabase.getInstance();
        myRef = database.getReference();


        btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, SignupActivity.class));
            }
        });


        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isFieldFilled()) {
                    Query q = myRef.child("users").orderByValue();

                    q.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            found = false;
                            User u = null;
                            for (DataSnapshot dst : dataSnapshot.getChildren()) {
                                u = dst.getValue(User.class);
                                u.setDatabaseKey(dst.getKey());

                                if (u.getEmail().equals(userEmail) && u.getPassword().equals(userPassword)) {
                                    found = true;
                                    break;
                                }

                            }
                            if (found) {
                                Intent i = new Intent(LoginActivity.this, MenuActivity.class);
                                if (cbKeep.isChecked())
                                {
                                    SharedPreferences.Editor editor =getSharedPreferences("userSettings",MODE_PRIVATE).edit();
                                    editor.putString("userKey",u.getDatabaseKey());
                                    editor.apply();
                                    editor.commit();
                                }
                                i.putExtra("loggedUser", u);
                                startActivity(i);
                            } else {
                                tvError.setVisibility(View.VISIBLE);
                                tvError.setText("Wrong credentials");
                            }

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });


                }
            }
        });
    }

    /**
     * validate user input
     * checks for valid email and password
     *
     * @return true for valid input, false otherwise
     */

    private boolean isFieldFilled() {
        userEmail = inputEmail.getText().toString().trim();
        userPassword = inputPassword.getText().toString().trim();
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(userEmail);

        if (!matcher.matches()) {
            tvError.setVisibility(View.VISIBLE);
            tvError.setText("email is required");
            return false;
        } else if (userPassword.length() < 3) {
            tvError.setVisibility(View.VISIBLE);
            tvError.setText("password is required");
            return false;
        }

        return true;
    }
}
