package com.example.erez1.androidfinalproject;
/**
 * This class is used to represent a game
 * created out of a room class
 */

import java.io.Serializable;
import java.util.ArrayList;

public class Game implements Serializable {
    private User u1;
    private User u2;
    private ArrayList<Answer> u1Answers;
    private ArrayList<Answer> u2Answers;
    private ArrayList<Question> questions;
    private String databaseKey;
    private int currQuestionIndex;
    private boolean isBothAnswered;

    /**
     * Constructor
     * @param u1 user 1
     * @param u2 user 2
     * @param questions game questions
     */
    public Game(User u1, User u2, ArrayList<Question> questions) {
        this.u1= u1;
        this.u2= u2;
        this.u1Answers = new ArrayList<>();
        this.u2Answers = new ArrayList<>();
        this.questions = questions;
        this.currQuestionIndex = 0;
        this.isBothAnswered = false;
    }

    /**
     * for firebase use
     */
    public Game(){}

    /**
     *  get the first user
     * @return user 1
     */
    public User getU1() {
        return u1;
    }

    /**
     * set the first user
     * @param u1 user 1
     */
    public void setU1(User u1) {
        this.u1 = u1;
    }

    /**
     * get the second user
     * @return user 2
     */
    public User getU2() {
        return u2;
    }

    /**
     * set the second user
     * @param u2 user 2
     */
    public void setU2(User u2) {
        this.u2 = u2;
    }

    /**
     * get user 1 answers
     * @return the answers user 1 made
     */
    public ArrayList<Answer> getU1Answers() {
        return u1Answers;
    }

    /**
     * set user 1 answers
     * @param u1Answers the answers user 1 made
     */
    public void setU1Answers(ArrayList<Answer> u1Answers) {
        this.u1Answers = u1Answers;
    }

    /**
     * get user 2 answers
     * @return answers user 2 made
     */
    public ArrayList<Answer> getU2Answers() {
        return u2Answers;
    }

    /**
     * set user 2 answers
     * @param u2Answers the answers user 2 made
     */
    public void setU2Answers(ArrayList<Answer> u2Answers) {
        this.u2Answers = u2Answers;
    }

    /**
     * get the game questions
     * @return arraylist of questions
     */
    public ArrayList<Question> getQuestions() {
        return questions;
    }

    /**
     * set the game questions
     * @param questions arraylist of questions
     */
    public void setQuestions(ArrayList<Question> questions) {
        this.questions = questions;
    }

    /**
     * get the database key
     * @return the database key
     */
    public String getDatabaseKey() {
        return databaseKey;
    }
    /**
     * set the database key
     * @param databaseKey the database key
     */
    public void setDatabaseKey(String databaseKey) {
        this.databaseKey = databaseKey;
    }

    /**
     * get the current question index
     * @return the current question index
     */
    public int getCurrQuestionIndex() {
        return currQuestionIndex;
    }

    /**
     * set the current question index
     * @param currQuestionIndex the current question index
     */
    public void setCurrQuestionIndex(int currQuestionIndex) {
        this.currQuestionIndex = currQuestionIndex;
    }

    /**
     * increase the current question index by 1
     */
    public void incCurrQuestionIndex(){this.currQuestionIndex++;}

    /**
     * indicates if both the users answered
     * @return true if both answered
     */
    public boolean isBothAnswered() {
        return isBothAnswered;
    }

    /**
     * set the both answered
     * @param bothAnswered boolean value to if both answered
     */
    public void setBothAnswered(boolean bothAnswered) {
        isBothAnswered = bothAnswered;
    }
}
