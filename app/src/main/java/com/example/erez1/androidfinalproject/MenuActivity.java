package com.example.erez1.androidfinalproject;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.support.v4.view.LayoutInflaterCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.style.BackgroundColorSpan;
import android.util.AttributeSet;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Field;

/**
 * This Activity is used for choosing application feature
 */
public class MenuActivity extends AppCompatActivity {
    private Button btnPlay, btnLeaderboard;
    private Toolbar mToolbar;
    private TextView tvTitle;
    private User user;

    /**
     * creates the UI of the activity
     * set toolbar
     *
     * @param savedInstanceState helps to create GUI
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        Intent i = getIntent();
        user = (User) i.getSerializableExtra("loggedUser");


        btnPlay = (Button) findViewById(R.id.btn__start_play);
        btnLeaderboard = (Button) findViewById(R.id.btn_leaderboard);
        tvTitle = (TextView) findViewById(R.id.tv_title);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(mToolbar);

        tvTitle.setText(tvTitle.getText() + "\n" + user.getName());

        btnPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MenuActivity.this, GameOptionsActivity.class);
                i.putExtra("loggedUser", user);
                startActivity(i);
            }
        });
        btnLeaderboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MenuActivity.this, LeaderboardActivity.class);
                i.putExtra("loggedUser", user);
                startActivity(i);
            }
        });

        if (GameActivity.gameManager != null){
            GameActivity.gameManager = null;
        }

    }

    /**
     * set menu layout
     *
     * @param menu menu instance
     * @return true for the menu to be displayed;  false it will not be shown.
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.options_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * handles the selected option from menu
     *
     * @param item the seleted option
     * @return boolean Return false to allow normal menu processing to proceed, true to consume it here.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.item_info:
                final Dialog infoDialog = new Dialog(MenuActivity.this);
                infoDialog.setContentView(R.layout.app_info_dialog);

                Button btnDismiss = (Button) infoDialog.findViewById(R.id.btn_dismiss);

                btnDismiss.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        infoDialog.dismiss();
                    }
                });
                infoDialog.show();

                break;
            case R.id.item_settings:
                Intent i = new Intent(MenuActivity.this, AccountSettingActivity.class);
                i.putExtra("loggedUser", user);
                startActivity(i);
                finish();
                break;
            case R.id.item_logout:
                //logout user
                startActivity(new Intent(MenuActivity.this, LoginActivity.class));
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);

    }
}
