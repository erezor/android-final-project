package com.example.erez1.androidfinalproject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * This class represent question
 * has an question text, category, difficulty an possible answers
 * used for attaching id to name in order to request it from the web api
 */


public class Question implements Serializable {
    private String que;
    private String correctAnswer;
    private ArrayList<String> wrongAnswers;
    private String category;
    private String difficulty;

    /**
     * Constructs Question instance
     *
     * @param que           question text
     * @param correctAnswer correct answer  text
     * @param wrongAnswers  wrong answers array of strings
     * @param category      category name
     * @param difficulty    difficulty level
     */
    public Question(String que, String correctAnswer, ArrayList<String> wrongAnswers, String category, String difficulty) {
        this.que = que;
        this.correctAnswer = correctAnswer;
        this.wrongAnswers = wrongAnswers;
        this.category = category;
        this.difficulty = difficulty;
    }

    /**
     * for firebase use
     */
    public Question() {
    }

    /**
     * get question text
     *
     * @return question text
     */
    public String getQue() {
        return que;
    }

    /**
     * set question text
     *
     * @param que question text
     */
    public void setQue(String que) {
        this.que = que;
    }

    /**
     * get correct answer
     *
     * @return correct answer text
     */
    public String getCorrectAnswer() {
        return correctAnswer;
    }

    /**
     * set correct answer
     *
     * @param correctAnswer the correct answer
     */
    public void setCorrectAnswer(String correctAnswer) {
        this.correctAnswer = correctAnswer;
    }

    /**
     * get the wrong answers array
     *
     * @return wrong answers strings arrray
     */
    public ArrayList<String> getWrongAnswers() {
        return wrongAnswers;
    }

    /**
     * set the wrong answers array
     *
     * @param wrongAnswers the wrong answers array
     */
    public void setWrongAnswers(ArrayList<String> wrongAnswers) {
        this.wrongAnswers = wrongAnswers;
    }

    /**
     * get the question category
     *
     * @return the question category
     */
    public String getCategory() {
        return this.category;
    }

    /**
     * set the question category
     *
     * @param category question category
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     * get question difficulty
     *
     * @return the question difficulty
     */
    public String getDifficulty() {
        return difficulty;
    }

    /**
     * set the question difficulty
     *
     * @param difficulty the question difficulty
     */
    public void setDifficulty(String difficulty) {
        this.difficulty = difficulty;
    }

}
