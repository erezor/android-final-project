package com.example.erez1.androidfinalproject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.io.Serializable;
import java.util.ArrayList;

public class GameHandlerU2 extends GameHandler implements Serializable {
    private User user;
    private FirebaseDatabase database;
    private DatabaseReference myRef;
    private Context context;
    private String gameKey;
    private Game game;

    /**
     * constructor and init super
     *
     * @param user    the user that uses the app
     * @param context activity that is using the class
     * @param gameKey firebase database game key
     * @param game    the game object
     */
    public GameHandlerU2(User user, Context context, String gameKey, Game game) {
        super(user, context, gameKey, game);
        this.user = user;
        this.context = context;
        this.database = FirebaseDatabase.getInstance();
        this.myRef = this.database.getReference();
        this.gameKey = gameKey;
        this.game = game;
    }

    /**
     * submit the answer to firebase
     *
     * @param isCorrect    is the user correct
     * @param answerTime   the amount of time that took him
     * @param currQuestion the current question that was answered
     */
    @Override
    public void finishRound(boolean isCorrect, float answerTime, Question currQuestion) {
        this.user.getStatistics().updateStatisticsQuestion(isCorrect, answerTime);
        this.user.getStatistics().calcScore();
        Answer ans = new Answer(this.user, isCorrect, currQuestion.getQue(), currQuestion.getCorrectAnswer());
        // handle first time
        if (this.game.getU2Answers() == null) {
            this.game.setU2Answers(new ArrayList<Answer>());
        }

        ArrayList<Answer> helper = this.game.getU2Answers();
        helper.add(0, ans);
        this.game.setU2Answers(helper);

        myRef.child("games").child(this.gameKey).child("u2").setValue(this.user);
        myRef.child("games").child(this.gameKey).child("u2Answers").setValue(this.game.getU2Answers());

    }

    /**
     * wait for both users to answer
     */
    @Override
    public void waitForBoth() {
        Query q = this.myRef.child("games").child(this.gameKey).child("bothAnswered");
        q.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                boolean isBothAnswered = dataSnapshot.getValue(boolean.class);
                if (isBothAnswered) {
                    while (!(GameHandlerU2.this.context instanceof PostAnswerActivity)) {
                        Intent res = new Intent(GameHandlerU2.this.context, PostAnswerActivity.class);
                        res.putExtra("loggedUser", user);
                        GameHandlerU2.this.context.startActivity(res);
                        ((Activity) GameHandlerU2.this.context).finish();
                    }
                    ((PostAnswerActivity) GameHandlerU2.this.context).applyUI(false);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    /**
     * Change the context that the class working from
     *
     * @param context the activity that uses the class
     */
    @Override
    public void setContext(Context context) {
        this.context = context;
    }

    /**
     * reset if both have answered to false
     * only u1 do that thats why does nothing
     */
    @Override
    public void resetBothHaveAnswered() {
        Log.e("TEST", "U1 Handle that");
    }

    @Override
    public void checkForBothHaveAnswered() {
        return;
    }
}
