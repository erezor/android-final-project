package com.example.erez1.androidfinalproject;

import java.io.Serializable;

/**
 * This class represents the user statistics
 * keep track with user results and calculate score
 */

public class UserStatistics implements Serializable {
    private int numOfCorrectAnswers;
    private int numOfTotalAnswers;
    private float averageAnswerTime;
    private int numOfGames;
    private float sumOfCorrectAnswers;

    /**
     * init all fields in 0
     */
    public UserStatistics() {
        this.numOfCorrectAnswers = 0;
        this.numOfTotalAnswers = 0;
        this.averageAnswerTime = 0;
        this.numOfGames = 0;
        this.sumOfCorrectAnswers = 0;
    }

    /**
     * calculate the user score
     *
     * @return the score
     */
    public int calcScore() {
        float score;
        score = (this.numOfTotalAnswers != 0) ? (this.numOfCorrectAnswers / this.numOfTotalAnswers) : 0;
        score = score * 100;
        score = averageAnswerTime != 0 ? score / averageAnswerTime : 0;
        score = score + numOfGames;
        score = score < 0 ? 0 : score;
        return (int) score;
    }

    /**
     * increment the num of correct answers by 1
     */
    public void incNumOfCorrectAnswers() {
        this.numOfCorrectAnswers += 1;
    }

    /**
     * increment the num of total answers by 1
     */
    public void incNumOfAnswers() {
        this.numOfTotalAnswers += 1;
    }

    /**
     * increment the num of games by 1
     */
    public void incNumOfGames() {
        this.numOfGames += 1;
    }

    /**
     * calculate the user average answer time
     */
    public void updateAverageAnswerTime() {
        this.averageAnswerTime = this.sumOfCorrectAnswers / this.numOfCorrectAnswers;
    }

    /**
     * update the user statistics after question was answered
     *
     * @param isCorrectAnswer if the user was correct
     * @param answerTime      the time that took him  to answer
     */
    public void updateStatisticsQuestion(boolean isCorrectAnswer, float answerTime) {
        this.incNumOfAnswers();

        if (isCorrectAnswer) {
            this.incNumOfCorrectAnswers();
            this.sumOfCorrectAnswers += answerTime;
            this.updateAverageAnswerTime();
        }
    }


    /**
     * for firebase use
     */
    public int getNumOfCorrectAnswers() {
        return numOfCorrectAnswers;
    }

    /**
     * for firebase use
     */
    public void setNumOfCorrectAnswers(int numOfCorrectAnswers) {
        this.numOfCorrectAnswers = numOfCorrectAnswers;
    }

    /**
     * for firebase use
     */
    public int getNumOfTotalAnswers() {
        return numOfTotalAnswers;
    }

    /**
     * for firebase use
     */
    public void setNumOfTotalAnswers(int numOfTotalAnswers) {
        this.numOfTotalAnswers = numOfTotalAnswers;
    }

    /**
     * for firebase use
     */
    public float getAverageAnswerTime() {
        return averageAnswerTime;
    }

    /**
     * for firebase use
     */
    public void setAverageAnswerTime(float averageAnswerTime) {
        this.averageAnswerTime = averageAnswerTime;
    }

    /**
     * for firebase use
     */
    public int getNumOfGames() {
        return numOfGames;
    }

    /**
     * for firebase use
     */
    public void setNumOfGames(int numOfGames) {
        this.numOfGames = numOfGames;
    }
}
