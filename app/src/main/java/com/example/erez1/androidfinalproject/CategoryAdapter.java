package com.example.erez1.androidfinalproject;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;


/**
 * Adapter to display all categories in spinner
 */
public class CategoryAdapter extends BaseAdapter {
    Context context;
    ArrayList<Category> categories;
    LayoutInflater inflter;

    public CategoryAdapter(Context applicationContext, ArrayList<Category> categories) {
        this.context = applicationContext;
        this.categories = categories;
        inflter = (LayoutInflater.from(applicationContext));
        Log.e("EREZ", String.valueOf(categories.size()));
    }

    /**
     * get the arraylist size
     * @return the arraylist size
     */
    @Override
    public int getCount() {
        return this.categories.size();
    }

    /**
     * get the category from spinner in i index
     * @param i the index
     * @return the category
     */
    @Override
    public Object getItem(int i) {
        return this.categories.get(i);
    }

    /**
     * get the category id from spinner in i index
     * @param i the index
     * @return the category id
     */
    @Override
    public long getItemId(int i) {
        return this.categories.get(i).getId();
    }

    /**
     * set the view of the spinner row for each line
     * @param i the current positions index
     * @param view the application row view
     * @param viewGroup not in use
     * @return the view after designed
     */
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.spinner_row, null);
        TextView tvName = view.findViewById(R.id.tv_category_name);
        tvName.setText(this.categories.get(i).getName());
        return view;
    }
}