package com.example.erez1.androidfinalproject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class RoomManager {

    private FirebaseDatabase database;
    private DatabaseReference myRef;
    private boolean roomFound;
    private User user;
    private Context context;
    private Room room;
    private Activity activity;
    private Game game;
    private Intent i;
    public RoomManager(User user, Context context) {
        this.database = FirebaseDatabase.getInstance();
        this.myRef = database.getReference();
        this.roomFound = false;
        this.user = user;
        this.context = context;
        this.activity = (Activity) context;
    }

    public void findRoom(final GameMatcher gameMatcher) {
        Query q = myRef.child("rooms").orderByKey();
        this.roomFound = false;
        q.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot dst : dataSnapshot.getChildren()) {
                    RoomManager.this.room = dst.getValue(Room.class);
                    if (RoomManager.this.room.getGameMatcher().equals(gameMatcher))
                        if (RoomManager.this.room.getStatus() == Room.WAITING_FOR_PLAYERS) {
                            roomFound = true;
                            RoomManager.this.room.setDatabaseKey(dst.getKey());
                            break;
                        }
                }
                // Join Room
                if (RoomManager.this.roomFound) {
                    Toast.makeText(context, "Room found! adding you to room", Toast.LENGTH_SHORT).show();
                    addPlayer();
                }
                // Create Room
                else {
                    Toast.makeText(context, "Room not found! creating new room", Toast.LENGTH_SHORT).show();
                    newRoom(gameMatcher);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void addPlayer() {
        room.setStatus(Room.IN_GAME);
        room.setU2(this.user);
        game = new Game(room.getU1(),room.getU2(),room.getQuestions());
        final DatabaseReference gameKey = myRef.child("games").push();
        room.setGameKey(gameKey.getKey());
        gameKey.setValue(game);
        this.myRef.child("rooms").child(room.getDatabaseKey()).setValue(room);
        Toast.makeText(context, "Game found", Toast.LENGTH_SHORT).show();
        i = new Intent(this.activity, GameActivity.class);
        i.putExtra("loggedUser", this.user);
        game.setDatabaseKey(gameKey.getKey());
        i.putExtra("game", game);
        this.activity.startActivity(i);
        this.activity.finish();
    }

    private void newRoom(final GameMatcher gameMatcher) {
        this.room = null;
        QuestionsApiHandler q = new QuestionsApiHandler(this.context, gameMatcher);
        q.getQuestions(new QuestionListAsyncResponse() {
            @Override
            public void processFinish(ArrayList<Question> questions) {
                RoomManager.this.room = new Room(RoomManager.this.user, questions, gameMatcher);
                final DatabaseReference recordRef = RoomManager.this.myRef.child("rooms").push();

                recordRef.setValue(RoomManager.this.room);
                RoomManager.this.room.setDatabaseKey(recordRef.getKey());

                Query query = myRef.child("rooms").child(recordRef.getKey()).orderByValue();
                query.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {
                            final Room updatedRoom = dataSnapshot.getValue(Room.class);
                            if (updatedRoom.getStatus() == Room.IN_GAME) {
                                Toast.makeText(context, "Another player joined! starting game", Toast.LENGTH_SHORT).show();
                                i = new Intent(RoomManager.this.activity, GameActivity.class);
                                i.putExtra("loggedUser", RoomManager.this.user);
                                Query q = myRef.child("games").child(updatedRoom.getGameKey()).orderByValue();
                                q.addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        game = dataSnapshot.getValue(Game.class);
                                        game.setDatabaseKey(dataSnapshot.getKey());
                                        i.putExtra("game", game);
                                        RoomManager.this.activity.startActivity(i);
                                        RoomManager.this.activity.finish();
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });

                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        });
    }
}