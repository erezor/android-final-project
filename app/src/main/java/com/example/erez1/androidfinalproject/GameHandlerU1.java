package com.example.erez1.androidfinalproject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.io.Serializable;
import java.util.ArrayList;

public class GameHandlerU1 extends GameHandler implements Serializable {
    private User user;
    private FirebaseDatabase database;
    private DatabaseReference myRef;
    private Context context;
    private String gameKey;
    private Game game;

    /**
     * constructor and init super
     *
     * @param user    the user that uses the app
     * @param context activity that is using the class
     * @param gameKey firebase database game key
     * @param game    the game object
     */
    public GameHandlerU1(User user, Context context, String gameKey, Game game) {
        super(user, context, gameKey, game);
        this.user = user;
        this.context = context;
        this.database = FirebaseDatabase.getInstance();
        this.myRef = this.database.getReference();
        this.gameKey = gameKey;
        this.game = game;
    }

    /**
     * submit the answer to firebase
     *
     * @param isCorrect    is the user correct
     * @param answerTime   the amount of time that took him
     * @param currQuestion the current question that was answered
     */
    @Override
    public void finishRound(boolean isCorrect, float answerTime, Question currQuestion) {
        this.user.getStatistics().updateStatisticsQuestion(isCorrect, answerTime);
        this.user.getStatistics().calcScore();
        Answer ans = new Answer(this.user, isCorrect, currQuestion.getQue(), currQuestion.getCorrectAnswer());
        // handle first time
        if (this.game.getU1Answers() == null) {
            this.game.setU1Answers(new ArrayList<Answer>());
        }

        ArrayList<Answer> helper = this.game.getU1Answers();
        helper.add(0, ans);
        this.game.setU1Answers(helper);

        myRef.child("games").child(this.gameKey).child("u1").setValue(this.user);
        myRef.child("games").child(this.gameKey).child("u1Answers").setValue(this.game.getU1Answers());
        myRef.child("games").child(this.gameKey).child("currQuestionIndex").setValue(this.game.getU1Answers().size());


    }
    @Override
    public void checkForBothHaveAnswered(){
        Query q = this.myRef.child("games").child(this.gameKey).child("u1Answers");
        q.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                final ArrayList<Answer> u1Answers = (ArrayList<Answer>)dataSnapshot.getValue();
                Query q1 = myRef.child("games").child(gameKey).child("u2Answers");
                q1.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        final ArrayList<Answer> u2Answers = (ArrayList<Answer>)dataSnapshot.getValue();
                        if(u1Answers != null && u2Answers != null) {
                            if (u1Answers.size() == u2Answers.size() && u1Answers.size() != 0) {
                                myRef.child("games").child(gameKey).child("bothAnswered").setValue(true);
                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    /**
     * wait for both users to answer
     */
    @Override
    public void waitForBoth() {
        checkForBothHaveAnswered();
        Query q = this.myRef.child("games").child(this.gameKey).child("bothAnswered");
        q.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                boolean isBothAnswered = dataSnapshot.getValue(boolean.class);
                if (isBothAnswered) {
                      if(!(GameHandlerU1.this.context instanceof PostAnswerActivity)){
                        Intent res = new Intent(GameHandlerU1.this.context, PostAnswerActivity.class);
                        res.putExtra("loggedUser", user);
                        GameHandlerU1.this.context.startActivity(res);
                        ((Activity)GameHandlerU1.this.context).finish();
                    }
                    ((PostAnswerActivity) GameHandlerU1.this.context).applyUI(true);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    /**
     * Change the context that the class working from
     *
     * @param context the activity that uses the class
     */
    @Override
    public void setContext(Context context) {
        this.context = context;
    }

    /**
     * reset if both have answered to false
     * only u1 do that
     */
    @Override
    public void resetBothHaveAnswered() {
        this.myRef.child("games").child(this.gameKey).child("bothAnswered").setValue(false);
    }
}
