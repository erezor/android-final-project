package com.example.erez1.androidfinalproject;

import android.support.annotation.NonNull;

import com.google.firebase.database.Exclude;

import java.io.Serializable;

/**
 * This class represent application user
 */

@SuppressWarnings("serial") //With this annotation we are going to hide compiler warnings

public class User implements Serializable, Comparable<User> {
    private String name;
    private String email;
    private String password;
    private UserStatistics statistics;
    private String databaseKey;

    /**
     * Constructs User object
     *
     * @param name     user name
     * @param email    user email
     * @param password user password
     */
    public User(String name, String email, String password) {
        this.name = name;
        this.email = email;
        this.password = password;
        statistics = new UserStatistics();
    }

    /**
     * for firebase  use
     */
    public User() {
    }

    /**
     * ge user name
     *
     * @return user name
     */
    public String getName() {
        return name;
    }

    /**
     * set user name
     *
     * @param name the user name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * get user statistics
     *
     * @return the user statistics
     */
    public UserStatistics getStatistics() {
        return statistics;
    }

    /**
     * get user email
     *
     * @return user email
     */
    public String getEmail() {
        return email;
    }

    /**
     * set user email
     *
     * @param email the user email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * get user password
     *
     * @return user password
     */
    public String getPassword() {
        return password;
    }

    /**
     * set user password
     *
     * @param password the user password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

    /**
     * get user firebase database key
     *
     * @return the user database key
     */
    public String getDatabaseKey() {
        return databaseKey;
    }

    /**
     * set the user database key
     *
     * @param databaseKey
     */
    public void setDatabaseKey(String databaseKey) {
        this.databaseKey = databaseKey;
    }

    /**
     * compare between two users by their score
     *
     * @param user other user
     * @return the difference between their score
     */
    @Override
    public int compareTo(@NonNull User user) {

        int compareage = ((User) user).getStatistics().calcScore();

        /*Descending order  */
        return compareage - this.getStatistics().calcScore();
    }
}
