package com.example.erez1.androidfinalproject;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * This class represent game rooom
 */
/*
0 - waiting for players
1 - in game
2 - game finished
 */

public class Room implements Serializable {
    private User u1;
    private User u2;
    private GameMatcher gameMatcher;
    private int status;
    private ArrayList<Question> questions;
    private String databaseKey;
    private String gameKey;

    public String getGameKey() {
        return gameKey;
    }

    public void setGameKey(String gameKey) {
        this.gameKey = gameKey;
    }

    public static final int WAITING_FOR_PLAYERS = 0;
    public static final int IN_GAME = 1;
    public static final int GAME_OVER = 2;
    /**
     * Constructs Room object
     * @param id room id
     * @param u1 User 1
     * @param u2 User 2
     * @param questions the questions of this room
     */
    public Room(User u1,  ArrayList<Question> questions,GameMatcher gameMatcher) {
        this.u1 = u1;
        this.u2 = null;
        this.status = WAITING_FOR_PLAYERS;
        this.questions = questions;
        this.gameMatcher = gameMatcher;
    }

    public Room() {}

    public User getU1() {
        return u1;
    }

    public void setU1(User u1) {
        this.u1 = u1;
    }

    public User getU2() {
        return u2;
    }

    public void setU2(User u2) {
        this.u2 = u2;
    }

    public GameMatcher getGameMatcher() {
        return gameMatcher;
    }

    public void setGameMatcher(GameMatcher gameMatcher) {
        this.gameMatcher = gameMatcher;
    }



    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(ArrayList<Question> questions) {
        this.questions = questions;
    }

    public String getDatabaseKey() {
        return databaseKey;
    }

    public void setDatabaseKey(String databaseKey) {
        this.databaseKey = databaseKey;
    }
}
