package com.example.erez1.androidfinalproject;

import java.io.Serializable;

/**
 * This class Stores the user, his current answer to a question, the question and if he was correct
 * used for connecting between the user and current question
 */

public class Answer implements Serializable {
    private User user;
    private boolean isUserCorrect;
    private String questionText;
    private String correctAnswer;

    /**
     * Constract an Answer
     *
     * @param user             The user that answered
     * @param isUserCorrect was he correct
     * @param questionText  the question text
     * @param correctAnswer the correct answer text
     */
    public Answer(User user, boolean isUserCorrect, String questionText, String correctAnswer) {
        this.user = user;
        this.isUserCorrect = isUserCorrect;
        this.questionText = questionText;
        this.correctAnswer = correctAnswer;
    }

    /**
     * for firebase use
     */
    public Answer() {
    }

    /**
     * get the correct answer text
     *
     * @return the correct answer text
     */
    public String getCorrectAnswer() {
        return correctAnswer;
    }

    /**
     * set the correct answer
     *
     * @param correctAnswer the correct answer text
     */
    public void setCorrectAnswer(String correctAnswer) {
        this.correctAnswer = correctAnswer;
    }


    /**
     * get the user
     *
     * @return user
     */
    public User getUser() {
        return user;
    }

    /**
     * set user
     *
     * @param user The user
     */
    public void setUser(User user){
        this.user = user;
    }

    /**
     * was the user correct?
     *
     * @return if the user is correct
     */
    public boolean isUserCorrect() {
        return isUserCorrect;
    }

    /**
     * set is user correct
     *
     * @param userCorrect is user correct
     */
    public void setUserCorrect(boolean userCorrect) {
        isUserCorrect = userCorrect;
    }




    /**
     * get the question text
     * @return the question text
     */
    public String getQuestionText() {
        return questionText;
    }

    /**
     * set the question text
     * @param questionText the question text
     */
    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }


}
