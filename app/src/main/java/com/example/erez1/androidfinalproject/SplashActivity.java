package com.example.erez1.androidfinalproject;

import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

/**
 * This Activity is used for displaying gif when app opens first
 * The activity make use of Glide libary in order to play gif video
 */
public class SplashActivity extends AppCompatActivity {

    private final int SPLASH_DISPLAY_LENGTH = 3000;
    private String databaseKey;
    private FirebaseDatabase database;
    private DatabaseReference myRef;
    private User user;
    CheckForInternetBroadcastReceiver receiver  = new CheckForInternetBroadcastReceiver();
    /**
     * creates the UI of the activity
     * display gif
     * pass user to login screen
     *
     * @param savedInstanceState helps to create GUI
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ImageView imageView = findViewById(R.id.imageView);
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference();
        Glide.with(this)
                .load(R.raw.giphy)
                .into(imageView);

        Intent svc=new Intent(this, MusicService.class);
        startService(svc);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                /* Create an Intent that will start the Menu-Activity. */
                SharedPreferences prefs = getSharedPreferences("userSettings", MODE_PRIVATE);
                databaseKey = prefs.getString("userKey", "");
                if (!databaseKey.equals("")) {
                    Query q = myRef.child("users").child(databaseKey).orderByValue();
                    q.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            SplashActivity.this.user = dataSnapshot.getValue(User.class);
                            SplashActivity.this.user.setDatabaseKey(dataSnapshot.getKey());
                            Intent menuIntent = new Intent(SplashActivity.this, MenuActivity.class);
                            menuIntent.putExtra("loggedUser",SplashActivity.this.user);
                            SplashActivity.this.startActivity(menuIntent);
                            SplashActivity.this.finish();
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                } else {
                    Intent mainIntent = new Intent(SplashActivity.this, LoginActivity.class);
                    SplashActivity.this.startActivity(mainIntent);
                    SplashActivity.this.finish();
                }
            }
        }, SPLASH_DISPLAY_LENGTH);
    }

    @Override
    protected void onStart() {
        super.onStart();
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(receiver,filter);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(receiver);
    }
}
