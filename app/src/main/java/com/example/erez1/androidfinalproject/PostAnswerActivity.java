package com.example.erez1.androidfinalproject;

import android.content.Intent;
import android.os.CountDownTimer;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.erez1.androidfinalproject.Answer;
import com.example.erez1.androidfinalproject.Game;
import com.example.erez1.androidfinalproject.GameActivity;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * This Activity is used for showing the current round result
 */
public class PostAnswerActivity extends AppCompatActivity {

    private TextView tvQuestionText, tvIsCorrect, tvCorrectAnswer, tvIsOtherCorrect;
    private ImageView ivIsCorrect;
    private FirebaseDatabase database;
    private DatabaseReference myRef;
    private Answer userAns, otherAns;
    private User user;
    private CountDownTimer ctd;

    private Game game;
    private String CORRECT_MESSAGE = " were correct";
    private String WRONG_MESSAGE = " were wrong";

    /**
     * creates the UI of the activity
     * set up buttons listener
     * set up countdownTimer to view result
     *
     * @param savedInstanceState helps to create GUI
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load_game);

        database = FirebaseDatabase.getInstance();
        myRef = database.getReference();

        Intent res = getIntent();
        user = (User) res.getSerializableExtra("loggedUser");

        if (GameActivity.gameManager == null) {
            this.user.getStatistics().incNumOfGames();
            this.myRef.child("users").child(this.user.getDatabaseKey()).setValue(this.user);
            Intent i = new Intent(PostAnswerActivity.this, MenuActivity.class);
            i.putExtra("loggedUser", user);
            startActivity(i);
            finish();
        }
        else {
            GameActivity.gameManager.setContext(PostAnswerActivity.this);
            GameActivity.gameManager.waitForBoth();
        }



        ctd = new CountDownTimer(5 * 1000, 1000) {
            @Override
            public void onTick(long l) {
            }

            @Override
            public void onFinish() {
                GameActivity.gameManager.resetBothHaveAnswered();
                Intent i = new Intent(PostAnswerActivity.this, GameActivity.class);
                i.putExtra("loggedUser", user);
                startActivity(i);
                finish();
            }
        };
    }

    /**
     * make sure to count down timer
     */
    @Override
    protected void onStop() {
        ctd.cancel();
        super.onStop();

    }

    /**
     * make sure to count down timer
     */
    @Override
    protected void onDestroy() {
        ctd.cancel();
        super.onDestroy();
    }

    /**
     * apply the user interface according
     * to the user answer
     *
     * @param isU1 is is user 1
     */
    public void applyUI(final boolean isU1) {
        Query q = myRef.child("games").child(GameActivity.gameManager.getGameKey());
        q.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                game = dataSnapshot.getValue(Game.class);
                if (isU1) {
                    userAns = game.getU1Answers().get(0);
                    otherAns = game.getU2Answers().get(0);
                } else {
                    userAns = game.getU2Answers().get(0);
                    otherAns = game.getU1Answers().get(0);
                }


                setContentView(R.layout.activity_post_answer);

                tvIsCorrect = (TextView) findViewById(R.id.tv_is_correct);
                tvIsOtherCorrect = (TextView) findViewById(R.id.tv_is_other_correct);
                tvQuestionText = (TextView) findViewById(R.id.tv_question_text);
                tvCorrectAnswer = (TextView) findViewById(R.id.tv_correct_answer);
                ivIsCorrect = (ImageView) findViewById(R.id.iv_is_correct);


                tvQuestionText.setText(userAns.getQuestionText());
                tvCorrectAnswer.setText("The correct Answer is " + userAns.getCorrectAnswer());

                if (userAns.isUserCorrect()) {
                    ivIsCorrect.setImageResource(R.drawable.correct);
                    tvIsCorrect.setText("You" + CORRECT_MESSAGE);
                } else {
                    ivIsCorrect.setImageResource(R.drawable.wrong);
                    tvIsCorrect.setText("You" + WRONG_MESSAGE);
                }

                if (otherAns.isUserCorrect()) {
                    tvIsOtherCorrect.setText(otherAns.getUser().getName() + CORRECT_MESSAGE);
                } else {
                    tvIsOtherCorrect.setText(otherAns.getUser().getName() + WRONG_MESSAGE);
                }

                ctd.start();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }
    @Override
    public void onBackPressed() {
        //do nothing
        return;
    }
}
