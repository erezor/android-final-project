package com.example.erez1.androidfinalproject;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import java.io.Serializable;

/**
 * this class manages the game handler
 */
public class GameManager implements Serializable {
    private Game game;
    private GameHandler gameHandler;
    private User user;
    private Context context;

    /**
     * constructor
     *
     * @param game    the game
     * @param user    the logged user
     * @param context the activity that uses it
     */
    public GameManager(Game game, User user, Context context) {
        this.game = game;
        this.user = user;
        this.context = context;

        if (user.getDatabaseKey().equals(game.getU1().getDatabaseKey())) {
            this.gameHandler = new GameHandlerU1(user, context, game.getDatabaseKey(), game);
        } else {
            this.gameHandler = new GameHandlerU2(user, context, game.getDatabaseKey(), game);
        }
    }

    /**
     * handles the finished game
     */
    public void finishGame() {
        this.gameHandler.finishGame();
    }

    /**
     * submit the answer to firebase
     *
     * @param isCorrect    is the user correct
     * @param answerTime   the amount of time that took him
     * @param currQuestion the current question that was answered
     */
    public void finishRound(boolean isCorrect, float answerTime, Question currQuestion) {
        Intent res = new Intent(this.context, PostAnswerActivity.class);
        res.putExtra("loggedUser", user);
        this.context.startActivity(res);
        this.gameHandler.finishRound(isCorrect, answerTime, currQuestion);
        ((Activity) this.context).finish();
    }

    /**
     * wait for both users to answer
     */
    public void waitForBoth() {
        this.gameHandler.waitForBoth();
    }

    /**
     * Change the context that the class working from
     *
     * @param context the activity that uses the class
     */
    public void setContext(Context context) {
        this.context = context;
        this.gameHandler.setContext(context);
    }

    /**
     * get firebase database key of game
     *
     * @return string of the key
     */
    public String getGameKey() {
        return this.game.getDatabaseKey();
    }

    /**
     * reset if both have answered to false
     * only u1 do that
     */

    public void resetBothHaveAnswered() {
        this.gameHandler.resetBothHaveAnswered();
    }
}
