package com.example.erez1.androidfinalproject;

import android.app.Activity;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Adapter to display all users in ListView on LeaderboardActivity
 */

public class LeaderboardAdapter extends ArrayAdapter<User> {

    private ArrayList<User> leaderboardUsers;
    private final Activity context;

    /**
     * Construct an Adapter, init super class
     *
     * @param context          The activity that contains the ListView that this adapter set on
     * @param leaderboardUsers the arraylist of users
     */
    public LeaderboardAdapter(Activity context, ArrayList<User> leaderboardUsers) {
        super(context, R.layout.leaderboard_row, leaderboardUsers);
        this.context = context;
        this.leaderboardUsers = leaderboardUsers;
    }

    /**
     * get the count of items in arraylist
     *
     * @return size of arraylist
     */
    @Override
    public int getCount() {
        return leaderboardUsers.size();
    }

    /**
     * get user from arraylist in selected index
     *
     * @param position the index
     * @return the user in position index in the arraylist
     */
    @Override
    public User getItem(int position) {
        return leaderboardUsers.get(position);
    }

    /**
     * not in use
     */
    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * is the listview empty
     *
     * @return false
     */
    @Override
    public boolean isEmpty() {
        return false;
    }

    /**
     * set the view of the ListView row for each line
     *
     * @param position    the current positions index
     * @param convertView the application row view
     * @param parent      not in use
     * @return the view after designed
     */
    public View getView(int position, View convertView, ViewGroup parent) {
        User u = this.leaderboardUsers.get(position);
        if (convertView == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(this.context);
            convertView = layoutInflater.inflate(R.layout.leaderboard_row, null);

            TextView tvName = convertView.findViewById(R.id.tv_name);
            TextView tvScore = convertView.findViewById(R.id.tv_score);
            ImageView imag = convertView.findViewById(R.id.iv);

            tvName.setText(u.getName());
            tvScore.setText(String.valueOf(u.getStatistics().calcScore()));

            switch (position) {
                case 0:
                    imag.setImageResource(R.drawable.ic_first_place);
                    break;
                case 1:
                    imag.setImageResource(R.drawable.ic_second_place);
                    break;
                case 2:
                    imag.setImageResource(R.drawable.ic_third_place);
                    break;
                default:
                    imag.setImageResource(R.drawable.ic_star);
                    break;
            }
        }
        return convertView;
    }


    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    @Override
    public boolean isEnabled(int position) {
        return true;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {
    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }


    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return leaderboardUsers.size();
    }


}
