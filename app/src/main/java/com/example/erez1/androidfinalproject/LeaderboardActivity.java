package com.example.erez1.androidfinalproject;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutCompat;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

/**
 * This Activity is used for showing the game leaderboareds
 * The activity connect with firebase in order to retrieve all users
 */
public class LeaderboardActivity extends AppCompatActivity {
    private Button btnExit, btnShowYou;
    private ListView lvLeaderboard;
    private ArrayList<User> leaderboardUsers;
    private ArrayList<User> helper;
    private ArrayList<User> newSize;
    private String[] names = {"Erez", "Or", "Raz", "Guy", "Lir", "Shahar", "Liron"};
    private String[] emails = {"email@example.com", "firstname.lastname@example.com", "email@subdomain.example.com",
            "firstname+lastname@example.com"};
    private String[] passwords = {"Erez211", "Or314", "Raz64w", "Guqdwy1221", "Li312r", "Shfsa21ahar", "Lcsaca21iron"};
    private FirebaseDatabase database;
    private DatabaseReference myRef;
    private User user;

    /**
     * creates the UI of the activity
     * set up buttons listener and ListView listener
     *
     * @param savedInstanceState helps to create GUI
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leaderboard);

        user = (User) getIntent().getSerializableExtra("loggedUser");
        leaderboardUsers = new ArrayList<User>();
        newSize = new ArrayList<User>();
        helper = new ArrayList<>(10);

        btnExit = (Button) findViewById(R.id.btn_exit);
        btnShowYou = (Button) findViewById(R.id.btn_show_you);
        lvLeaderboard = (ListView) findViewById(R.id.lv);

        database = FirebaseDatabase.getInstance();
        myRef = database.getReference();

        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        btnShowYou.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog statDialog = new Dialog(LeaderboardActivity.this);
                statDialog.setContentView(R.layout.stat_dialog);
                TextView tvStatName = (TextView) statDialog.findViewById(R.id.tv_stats_name);
                TextView tvStatScore = (TextView) statDialog.findViewById(R.id.tv_stats_score);
                Button btnStatDismiss = (Button) statDialog.findViewById(R.id.btn_stats_dismiss);

                btnStatDismiss.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        statDialog.dismiss();
                    }
                });

                tvStatName.setText(user.getName() + "'s Score is:");
                tvStatScore.setText(String.valueOf(user.getStatistics().calcScore()));
                statDialog.show();

                //set width
                Window window = statDialog.getWindow();
                window.setLayout(LinearLayoutCompat.LayoutParams.MATCH_PARENT, LinearLayoutCompat.LayoutParams.WRAP_CONTENT);
            }
        });
        lvLeaderboard.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                leaderboardDialogHandler(helper.get(i));
            }
        });

        loadUsers();

    }

    /**
     * insert users from firebase to arrayList
     */
    private void loadUsers() {
        leaderboardUsers.clear();
        Query q = myRef.child("users").orderByValue();
        q.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot dst : dataSnapshot.getChildren()) {
                    User u = dst.getValue(User.class);
                    Log.e("Erez", u.getEmail() + String.valueOf(u.getStatistics().calcScore()));
                    leaderboardUsers.add(u);
                }
                filterTop10();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    /**
     * filter to the 10 highest score users and display them in ListView
     */
    private void filterTop10() {
        Log.e("Erez", String.valueOf(leaderboardUsers.size()));

        Collections.sort(leaderboardUsers);
        for (int i = 0; i < 10; i++) {
            helper.add(i, leaderboardUsers.get(i));
            Log.e("OOO", String.valueOf(helper.get(i).getStatistics().calcScore()));
        }
        refreshLv(helper);

    }


    /**
     * display a dialog with the amount of users with the
     * same score as the user selected
     *
     * @param u the selected user
     */
    private void leaderboardDialogHandler(User u) {

        //create dialog
        final Dialog listviewDialog = new Dialog(LeaderboardActivity.this);
        listviewDialog.setContentView(R.layout.test_dialog_layout);
        listviewDialog.show();

        //connect textview and button
        TextView tvTask = (TextView) listviewDialog.findViewById(R.id.tv_task);
        Button btnDismiss = (Button) listviewDialog.findViewById(R.id.btn_dismiss);


        //dismiss dialog
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listviewDialog.dismiss();
            }
        });

        //count users
        int len = 0;
        for (int i = 0; i < helper.size(); i++) {
            if (helper.get(i).getStatistics().calcScore() == u.getStatistics().calcScore()) {
                len += 1;
            }
        }

        //display result
        tvTask.setText("There is " + String.valueOf(len) + " users with the same score");
    }

    /**
     * refresh the ListView adapter after changes
     *
     * @param arr the users arraylist to create adapter to it
     */
    private void refreshLv(ArrayList<User> arr) {
        LeaderboardAdapter adp = new LeaderboardAdapter(LeaderboardActivity.this, arr);
        lvLeaderboard.setAdapter(adp);
    }

}
