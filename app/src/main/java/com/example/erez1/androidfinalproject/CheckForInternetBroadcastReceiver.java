package com.example.erez1.androidfinalproject;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.widget.Toast;

public class CheckForInternetBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        if(ConnectivityManager.CONNECTIVITY_ACTION.equals(intent.getAction())){
            boolean noConnection = intent.getBooleanExtra(
                    ConnectivityManager.EXTRA_NO_CONNECTIVITY, false
            );

            if(noConnection){
                Toast.makeText(context, "No connection to internet, connect to " +
                        "internet in order to continue", Toast.LENGTH_SHORT).show();
            }
            else{
                Toast.makeText(context, "connected to internet", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
