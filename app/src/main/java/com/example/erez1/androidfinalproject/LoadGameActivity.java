package com.example.erez1.androidfinalproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import java.util.ArrayList;
/**
 * This Activity is a waiting room while the game loads up
 * The activity connect with web api in order to get the questions
 */
public class LoadGameActivity extends AppCompatActivity {
    private User user;
    private RoomManager roomManager;
    /**
     * creates the UI of the activity
     * display progress bar to indicate loading
     * use api handler in order to get questions
     * pass arraylist of questions to game
     *
     * @param savedInstanceState helps to create GUI
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load_game);

        Intent ii = getIntent();
        user = (User) ii.getSerializableExtra("loggedUser");

        int amount = ii.getIntExtra("amount", -1);
        int categoryID = ii.getIntExtra("categoryID", -1);
        String level = ii.getStringExtra("level");

        GameMatcher gameMatcher = new GameMatcher(categoryID,amount,level);
        roomManager = new RoomManager(user,LoadGameActivity.this);
        roomManager.findRoom(gameMatcher);
    }
    @Override
    public void onBackPressed() {
        return;
    }
}
