package com.example.erez1.androidfinalproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.database.Query;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This Activity is used for changing user credentials
 * The activity connect with firebase in order to update user in the database
 */
public class AccountSettingActivity extends AppCompatActivity {
    private Button btnUpdate, btnCancel;
    private EditText etName, etEmail, etPassword, etConfirmPassword;
    private String name, email, password, confirmPassword;
    private User user;
    private FirebaseDatabase database;
    private DatabaseReference myRef;


    /**
     * creates the UI of the activity
     * set up buttons listener
     * updates the firebase database
     *
     * @param savedInstanceState helps to create GUI
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_setting);


        Intent i = getIntent();
        user = (User) i.getSerializableExtra("loggedUser");

        btnUpdate = (Button) findViewById(R.id.btn_update);
        btnCancel = (Button) findViewById(R.id.btn_cancel_changes);
        etName = (EditText) findViewById(R.id.et_display_name);
        etEmail = (EditText) findViewById(R.id.et_email);
        etPassword = (EditText) findViewById(R.id.et_password);
        etConfirmPassword = (EditText) findViewById(R.id.et_confirm_password);

        fillData();

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(AccountSettingActivity.this, MenuActivity.class);
                i.putExtra("loggedUser", user);
                startActivity(i);
            }
        });

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                name = etName.getText().toString();
                email = etEmail.getText().toString().trim();
                password = etPassword.getText().toString().trim();
                confirmPassword = etConfirmPassword.getText().toString().trim();

                boolean isChanged = false;

                //update user
                if (!name.equals(user.getName())) {
                    if (name.length() == 0) {
                        isChanged = false;
                        Toast.makeText(AccountSettingActivity.this, "Name is a must!", Toast.LENGTH_SHORT).show();
                    } else {
                        user.setName(name);
                        isChanged = true;
                    }
                }
                if (!email.equals(user.getEmail())) {
                    Pattern pattern = Pattern.compile(LoginActivity.regex);
                    Matcher matcher = pattern.matcher(email);
                    if (!matcher.matches()) {
                        isChanged = false;

                        Toast.makeText(AccountSettingActivity.this, "valid email is a must!", Toast.LENGTH_SHORT).show();
                    } else {
                        user.setEmail(email);
                        isChanged = true;
                    }
                }
                if (!password.equals(user.getPassword())) {
                    if (password.length() < 3 && password.length() > 0) {
                        isChanged = false;
                        Toast.makeText(AccountSettingActivity.this, "password is too short", Toast.LENGTH_SHORT).show();
                    } else if (password.length() > 3) {
                        if (password.equals(confirmPassword)) {
                            user.setPassword(password);
                            isChanged = true;
                        } else {
                            Toast.makeText(AccountSettingActivity.this, "passwords doesn't match", Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                if (isChanged) {
                    database = FirebaseDatabase.getInstance();
                    myRef = database.getReference();
                    myRef.child("users").child(user.getDatabaseKey()).setValue(user);
                    Intent i = new Intent(AccountSettingActivity.this, MenuActivity.class);
                    i.putExtra("loggedUser", user);
                    startActivity(i);
                }


            }
        });


    }

    /**
     * get the user name and email and put it inside the correct EditText's
     */
    private void fillData() {
        etName.setText(user.getName());
        etEmail.setText(user.getEmail());
    }
}
