package com.example.erez1.androidfinalproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

/**
 * This Activity is used for choosing the game options
 * The activity connect with firebase in order to get the categories options
 */
public class GameOptionsActivity extends AppCompatActivity {
    private Spinner spCategory, spDifficulty, spAmount;
    private Button btnPlay;
    private String[] amounts;
    private String[] difficultyLevels;
    private ArrayList<Category> categories;
    private FirebaseDatabase database;
    private DatabaseReference myRef;
    private User user;

    private int categoryID, amount;
    private String level;

    /**
     * creates the UI of the activity
     * set up buttons listener
     *
     * @param savedInstanceState helps to create GUI
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_options);

        user = (User) getIntent().getSerializableExtra("loggedUser");

        spCategory = (Spinner) findViewById(R.id.sp_category);
        spDifficulty = (Spinner) findViewById(R.id.sp_difficulty);
        spAmount = (Spinner) findViewById(R.id.sp_amount);
        btnPlay = (Button) findViewById(R.id.btn_play);
        categories = new ArrayList<>();

        database = FirebaseDatabase.getInstance();
        myRef = database.getReference();

        setOptions();

        btnPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (spCategory.getSelectedItemPosition() >= 0) {
                    amount = Integer.parseInt(spAmount.getSelectedItem().toString());
                    level = spDifficulty.getSelectedItem().toString();
                    categoryID = categories.get(spCategory.getSelectedItemPosition()).getId();
                    Intent i = new Intent(GameOptionsActivity.this, LoadGameActivity.class);
                    i.putExtra("loggedUser", user);
                    i.putExtra("level", level);
                    i.putExtra("amount", amount);
                    i.putExtra("categoryID", categoryID);
                    startActivity(i);
                }
            }
        });
    }


    /**
     * set the options for the trivia game
     * get all categories from firebase
     * set the amount of questions, difficulties levels and categories
     * creates and set adapter for each spinner
     */
    private void setOptions() {
        amounts = new String[15];
        for (int i = 0; i < amounts.length; i++) {
            amounts[i] = String.valueOf(i + 1);
        }

        difficultyLevels = new String[3];
        difficultyLevels[0] = "easy";
        difficultyLevels[1] = "medium";
        difficultyLevels[2] = "hard";


        ArrayAdapter<String> adpDifficulties = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, difficultyLevels);
        ArrayAdapter<String> adpAmounts = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, amounts);

        spAmount.setAdapter(adpAmounts);
        spDifficulty.setAdapter(adpDifficulties);

        Query q = myRef.child("categories").orderByValue();
        q.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot dst : dataSnapshot.getChildren()) {
                    Category c = dst.getValue(Category.class);
                    categories.add(c);
                }
                categories.add(0, new Category(-1, "Random"));

                CategoryAdapter adpCategories = new CategoryAdapter(GameOptionsActivity.this, categories);
                spCategory.setAdapter(adpCategories);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }
}
