package com.example.erez1.androidfinalproject;

import android.content.Context;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import java.io.Serializable;
import java.util.ArrayList;


/**
 * semi abstract class to handle the progress of the game
 * communicates with firebase
 */
abstract public class GameHandler implements Serializable {
    private User user;
    private FirebaseDatabase database;
    private DatabaseReference myRef;
    private Context context;
    private String gameKey;
    private Game game;

    /**
     * constructor
     * @param user the user that uses the app
     * @param context activity that is using the class
     * @param gameKey firebase database game key
     * @param game the game object
     */
    public GameHandler(User user, Context context,String gameKey,Game game){
        this.user = user;
        this.context = context;
        this.database = FirebaseDatabase.getInstance();
        this.myRef = this.database.getReference();
        this.gameKey = gameKey;
        this.game = game;
    }

    /**
     * handles the finish of the game
     * update user statistics
     */
    public void finishGame() {
        this.user.getStatistics().incNumOfGames();
        this.myRef.child("users").child(this.user.getDatabaseKey()).setValue(this.user);
    }

    /**
     * create two non single value listeners that
     * listen on the two answers arrays and check if both answered
     * and update firebase
     */

    abstract public void checkForBothHaveAnswered();

    /**
     * Change the context that the class working from
     *
     * @param context the activity that uses the class
     */
    abstract public void setContext(Context context);

    /**
     * submit the answer to firebase
     * @param isCorrect is the user correct
     * @param answerTime the amount of time that took him
     * @param currQuestion the current question that was answered
     */
    abstract public void finishRound(boolean isCorrect, float answerTime, Question currQuestion);

    /**
     * wait for both users to answer
     */
    abstract public void waitForBoth();

    /**
     * reset if both have answered to false
     * only u1 do that
     */
    abstract public void resetBothHaveAnswered();
}
