package com.example.erez1.androidfinalproject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Random;

/**
 * This Activity is the game itself
 * display the question and the possible answers
 * measure time that takes the user to answer also check if he is correct
 * The activity connect with firebase in order to update user answers in database
 */
public class GameActivity extends AppCompatActivity {
    private User user;
    private Question currQuestion;
    private ArrayList<Button> btns;
    private TextView tvQuestion;
    private CountDownTimer ctd;
    private FirebaseDatabase database;
    private DatabaseReference myRef;
    private CircularProgressBar circularProgressBar;
    private Game game;
    private String gameKey;
    private long startTime;
    private boolean hasUserAnswer = false;
    public static GameManager gameManager;

    /**
     * creates the UI of the activity
     * set up buttons listener
     * set up the countdown timer and his progress bar
     * updates the firebase database
     *
     * @param savedInstanceState helps to create GUI
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        startTime = System.currentTimeMillis();
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference();

        circularProgressBar = (CircularProgressBar) findViewById(R.id.pb);
        int animationDuration = 15000; //15s
        circularProgressBar.setProgressWithAnimation(100, animationDuration);

        btns = new ArrayList<Button>();

        tvQuestion = (TextView) findViewById(R.id.tv_question);
        btns.add((Button) findViewById(R.id.btn_answer1));
        btns.add((Button) findViewById(R.id.btn_answer2));
        btns.add((Button) findViewById(R.id.btn_answer3));
        btns.add((Button) findViewById(R.id.btn_answer4));


        ctd = new CountDownTimer(15 * 1000, 1000) {
            @Override
            public void onTick(long l) {
            }

            @Override
            public void onFinish() {
                if (!hasUserAnswer) {
                    gameManager.finishRound(false, 0, currQuestion);
                }
            }
        };

        user = (User) getIntent().getSerializableExtra("loggedUser");


        if (gameManager == null) {
            game = (Game) getIntent().getSerializableExtra("game");
            gameManager = new GameManager(game, user, this);
        } else {
            gameManager.setContext(this);
        }


        gameKey = gameManager.getGameKey();


        myRef.child("games").child(gameKey).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                game = dataSnapshot.getValue(Game.class);
                if (game == null || game.getCurrQuestionIndex() >= game.getQuestions().size()) {
                    gameManager.finishGame();
                    Intent i = new Intent(GameActivity.this, MenuActivity.class);
                    i.putExtra("loggedUser", user);
                    startActivity(i);
                } else {
                    createAnswerListeners();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });


    }

    /**
     * randomize the location of the answers in the buttons
     * set answer text on buttons
     */
    private void setAnswers() {
        String[] answers = new String[4];
        answers[0] = currQuestion.getCorrectAnswer();
        answers[1] = currQuestion.getWrongAnswers().get(0);
        answers[2] = currQuestion.getWrongAnswers().get(1);
        answers[3] = currQuestion.getWrongAnswers().get(2);

        Collections.shuffle(btns);

        for (int i = 0; i < 4; i++) {
            btns.get(i).setText(answers[i]);
        }
        tvQuestion.setText(currQuestion.getQue());
    }


    /**
     * creates listeners for the answeres buttons
     */
    private void createAnswerListeners() {
        GameActivity.this.currQuestion = game.getQuestions().get(game.getCurrQuestionIndex());
        setAnswers();
        View.OnClickListener submitAnswer = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                float answerTime = (System.currentTimeMillis() - GameActivity.this.startTime) / 1000;
                Button curr = (Button) findViewById(view.getId());
                boolean isUserCorrect = curr.getText().toString().equals(currQuestion.getCorrectAnswer());
                hasUserAnswer = true;
                gameManager.finishRound(isUserCorrect, answerTime, currQuestion);
            }
        };
        for (int i = 0; i < btns.size(); i++) {
            btns.get(i).setOnClickListener(submitAnswer);
        }
        ctd.start();
    }


    /**
     * handles the stop of the activity
     * make sure to cancel count down timer
     */
    @Override
    protected void onStop() {
        ctd.cancel();
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        //do nothing
        return;
    }

}
