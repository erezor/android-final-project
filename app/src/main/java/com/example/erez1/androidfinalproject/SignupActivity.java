package com.example.erez1.androidfinalproject;

import android.app.Dialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.database.Query;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This Activity is used for logging into the application
 * The activity connect with firebase in order to register user
 */
public class SignupActivity extends AppCompatActivity {

    private String userEmail, userPassword, userName;
    private EditText inputEmail, inputPassword, inputName;
    private Button btnSignup, btnLogin;
    private FirebaseDatabase database;
    private DatabaseReference myRef;
    private Boolean userFound;
    private TextView tvError;
    private User newUser;
    private static final String regex = "^(.+)@(.+)$";

    /**
     * creates the UI of the activity
     * set up buttons listener
     *
     * @param savedInstanceState helps to create GUI
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        inputEmail = (EditText) findViewById(R.id.input_email);
        inputPassword = (EditText) findViewById(R.id.input_password);
        inputName = (EditText) findViewById(R.id.input_name);
        btnLogin = (Button) findViewById(R.id.btn_login);
        btnSignup = (Button) findViewById(R.id.btn_signup);
        tvError = (TextView) findViewById(R.id.tv_error);


        database = FirebaseDatabase.getInstance();
        myRef = database.getReference();

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isFieldFilled()) {
                    userFound = false;
                    Query q = myRef.child("users").orderByValue();

                    q.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            for (DataSnapshot dst : dataSnapshot.getChildren()) {
                                User u = dst.getValue(User.class);
                                if (u.getEmail().equals(userEmail)) {
                                    userFound = true;
                                }

                            }

                            if (userFound) {
                                tvError.setVisibility(View.VISIBLE);
                                tvError.setText("email is already in use");
                            } else {
                                newUser = new User(userName, userEmail, userPassword);
                                signupDialogHandler();
                            }

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

                }
            }
        });


    }

    /**
     * validate user input
     * checks for valid email, name and password
     *
     * @return true for valid input, false otherwise
     */
    private boolean isFieldFilled() {
        userEmail = inputEmail.getText().toString().trim();
        userPassword = inputPassword.getText().toString().trim();
        userName = inputName.getText().toString();
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(userEmail);


        if (!matcher.matches()) {
            tvError.setVisibility(View.VISIBLE);
            tvError.setText("email is required");
            return false;
        }
        if (userPassword.length() < 3) {
            tvError.setVisibility(View.VISIBLE);
            tvError.setText("password is required, length must be 3 at least");
            return false;
        }
        if (userName.isEmpty()) {
            tvError.setVisibility(View.VISIBLE);
            tvError.setText("name is required");
            return false;
        }

        return true;
    }

    /**
     * display dialog in order to check if user is sure
     */
    private void signupDialogHandler() {

        final Dialog signupDialog = new Dialog(SignupActivity.this);
        signupDialog.setContentView(R.layout.signup_dialog_layout);
        signupDialog.show();

        Button btnConfirm = (Button) signupDialog.findViewById(R.id.btn_confirm);
        Button btnCancel = (Button) signupDialog.findViewById(R.id.btn_cancel);

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myRef.child("users").push().setValue(newUser);
                finish();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signupDialog.dismiss();
            }
        });
    }
}