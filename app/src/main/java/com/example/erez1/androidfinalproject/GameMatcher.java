package com.example.erez1.androidfinalproject;


import java.io.Serializable;

/**
 * handles the matchmaking of the game
 */
public class GameMatcher implements Serializable {
    private int categoryId;
    private int questionAmount;
    private String difficulty;

    /**
     * constructor
     *
     * @param categoryId     the category id
     * @param questionAmount the amount of questions
     * @param difficulty     the difficulty of the questions
     */
    public GameMatcher(int categoryId, int questionAmount, String difficulty) {
        this.categoryId = categoryId;
        this.questionAmount = questionAmount;
        this.difficulty = difficulty;
    }


    public GameMatcher() {
    }

    /**
     * getter
     */
    public int getCategoryId() {
        return categoryId;
    }

    /**
     * setter
     *
     * @param categoryId
     */
    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    /**
     * getter
     */
    public int getQuestionAmount() {
        return questionAmount;
    }

    /**
     * setter
     *
     * @param questionAmount
     */
    public void setQuestionAmount(int questionAmount) {
        this.questionAmount = questionAmount;
    }

    /**
     * getter
     */
    public String getDifficulty() {
        return difficulty;
    }

    /**
     * setter
     *
     * @param difficulty
     */
    public void setDifficulty(String difficulty) {
        this.difficulty = difficulty;
    }

    @Override
    public boolean equals(Object gameMatcher) {
        if (gameMatcher == null) return false;
        if (gameMatcher == this) return true;
        if (!(gameMatcher instanceof GameMatcher)) return false;
        GameMatcher gameMatcher1 = (GameMatcher) gameMatcher;
        return this.getCategoryId() == gameMatcher1.getCategoryId() && this.getDifficulty().equals(gameMatcher1.getDifficulty())
                && this.getQuestionAmount() == gameMatcher1.getQuestionAmount();
    }
}
